package fr.uavignon.ceri.tp3;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonAPI {

    @GET("collection")
    Call<ResponseBody> getItems();

}
