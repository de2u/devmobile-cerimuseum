package fr.uavignon.ceri.tp3;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.moshi.Moshi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class MainActivity extends AppCompatActivity {

    private TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textViewResult = findViewById(R.id.textView);

        Moshi moshi = new Moshi.Builder().build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                //.addConverterFactory(MoshiConverterFactory.create(moshi))
                .build();

        JsonAPI jsonAPI = retrofit.create(JsonAPI.class);

        Call<ResponseBody> call = jsonAPI.getItems();

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (!response.isSuccessful()) {
                    textViewResult.setText("Code: " + response.code());
                }

                ResponseBody items = response.body();

//                for (Item item : items) {
//                    String content = "";
//                    content += "Name: " + item.getName() + "\n";
//                    content += "Desc: " + item.getDescription() + "\n\n";
//
//                    textViewResult.append(content);
//                }
                try {
                    textViewResult.setText(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_update_all) {
            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Interrogation à faire du service web",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}