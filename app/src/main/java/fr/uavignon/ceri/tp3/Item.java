package fr.uavignon.ceri.tp3;

import java.util.ArrayList;

public class Item {
    private String id;
    private String name;
    private String[] categories;
    private String description;
    private int[] timeFrame;
    private int year;
    private String brand;
    private String[] technicalDetails;
    private String[] pictures;
    private boolean working;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String[] getCategories() {
        return categories;
    }

    public String getDescription() {
        return description;
    }

    public int[] getTimeFrame() {
        return timeFrame;
    }

    public int getYear() {
        return year;
    }

    public String getBrand() {
        return brand;
    }

    public String[] getTechnicalDetails() {
        return technicalDetails;
    }

    public String[] getPictures() {
        return pictures;
    }

    public boolean isWorking() {
        return working;
    }
}
